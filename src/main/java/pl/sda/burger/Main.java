package pl.sda.burger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    private Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception {
        showInitDialog(primaryStage);
    }

    private void showInitDialog(Stage primaryStage) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Potwierdz gotowosc!");
        alert.setHeaderText("Prosze potwierdzic gotowosc do gotowania");
        alert.setContentText("Potwierdzasz?");
        alert.showAndWait()
                .filter(e -> e.equals(ButtonType.OK))
                .ifPresent(e -> showStage(primaryStage));
    }

    private void showStage(Stage primaryStage) {
        final Parent root = loadUi();
        primaryStage.setTitle("Burger maker");
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.show();
    }

    private Parent loadUi() {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
            root = loader.load();
            controller = loader.getController();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return root;
    }

    @Override
    public void stop() throws Exception {
        controller.shutdown();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
