package pl.sda.burger.service;

import pl.sda.burger.model.Order;
import pl.sda.burger.model.enums.Ingredient;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class CurrentPlateHandler {
    private OrderValidator orderValidator;
    private Stack<Ingredient> ingredientsOnPlate;

    public CurrentPlateHandler(OrderValidator orderValidator) {
        this.orderValidator = orderValidator;
    }


    public void addIngredient(Ingredient ingredient) {

    }

    public List<Ingredient> getIngredients() {
        return Collections.list(ingredientsOnPlate.elements());
    }

    public void clearPlate() {

    }

    public boolean validate(Order orderToValidate) {
        return orderValidator.validate(ingredientsOnPlate, orderToValidate);
    }


}
