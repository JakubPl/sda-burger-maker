package pl.sda.burger;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import pl.sda.burger.service.CurrentPlateHandler;
import pl.sda.burger.service.OrderGenerator;
import pl.sda.burger.service.OrderValidator;

public class Controller {
    @FXML
    private Label score;
    @FXML
    private Label time;
    @FXML
    private Label order;
    @FXML
    private Label onPlate;

    private CurrentPlateHandler currentPlateHandler;
    private OrderGenerator orderGenerator;
    private OrderValidator orderValidator;

    private int gameScore;


    @FXML
    public void initialized() {
        System.out.println("Zainicjalizowano!");
        orderValidator = new OrderValidator();
        orderGenerator = new OrderGenerator();
        currentPlateHandler = new CurrentPlateHandler(orderValidator);
    }

    public void sesameRollClicked(MouseEvent mouseEvent) {
        System.out.println("Bulka sezam!");
        gameScore++;
        //tak ustawia sie tekst:
        score.setText("Wynik:" + gameScore);
    }

    public void wheatRollClicked(MouseEvent mouseEvent) {
        System.out.println("Bulka zytnia!");

    }

    public void grahamRollClicked(MouseEvent mouseEvent) {
        System.out.println("Bulka graham!");
    }

    public void meatClicked(MouseEvent mouseEvent) {
        System.out.println("Mieso!");
    }

    public void lettuceClicked(MouseEvent mouseEvent) {
        System.out.println("Salata!");
    }

    public void tomatoClicked(MouseEvent mouseEvent) {
        System.out.println("Pomidor!");
    }

    public void cucumberClicked(MouseEvent mouseEvent) {
        System.out.println("Ogorek!");
    }

    public void pepperClicked(MouseEvent mouseEvent) {
        System.out.println("Papryka!");
    }

    public void cheeseClicked(MouseEvent mouseEvent) {
        System.out.println("Ser!");
    }

    public void clearClicked(MouseEvent mouseEvent) {
        System.out.println("Wyczysc talerz!");
    }

    public void readyClicked(MouseEvent mouseEvent) {
        showScoreDialog();
        System.out.println("Gotowe do serwowania!");
    }

    public void showScoreDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Koniec gry");
        alert.setHeaderText("Koniec gry");
        alert.setContentText("Twoj wynik to:" + gameScore);
        alert.show();
    }

    public void shutdown() {
        //tutaj akcje konieczne do przeprowadzenie przy wylaczaniu (np. zwalnianie zasobow itd.)
    }
}
