package pl.sda.burger.model.enums;

public enum Ingredient {
    CUCUMBER, LETTUCE, TOMATO, MEAT, PEPPER, CHEESE, WHEAT_ROLL, SESAME_ROLL, GRAHAM_ROLL
}
